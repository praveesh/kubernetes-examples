# Kubernetes Examples

## Description

Like the name said, in this project we will provide simple examples to explain how to use kubernetes to deploy your applications.
In all these examples we will use the Google Cloud Platform (gcp) because it offers a free trial period. For 1 year google is offering 300 dollar free uses. Go ahead and create your account :

-[google cloud platform](https://console.cloud.google.com)

We will provide various examples. You can find below some of the topics that we will discuss :

- Deploy your first app
- Use Google Load Balancer
- Use a Node Port
- Use a Cluster IP
- Use google ingress
- Use nginx ingress
- Use volumes
- Secure your app using https protocol
- and more ...

If you didn't use kubernetes before, try to follow this order :

1. gke-examples
2. ingress-examples
3. watch-shop-gke
4. watch-shop-nginx

## Requirements

Before beginning, you have to :

1. Have a google cloud account

2. Install the google cloud SDK. This link may help you :

- [https://cloud.google.com/sdk/docs/?hl=en](https://cloud.google.com/sdk/docs/?hl=en)

3. Log into your google account and create your first project

4. Now we will create our first kubernetes cluster. You can do it using command lines (like i will explain below) or using the google console (GUI).

```sh
    $ gcloud auth login
```

list all your projects using this command :

    $ gcloud projects list

check the documentation to see more : https://cloud.google.com/sdk/gcloud/reference/projects/list

set your project id :

    $ gcloud config set project [your-project-id]
    $ gcloud config set compute/zone [COMPUTE_ENGINE_ZONE]

create your first cluster :

    $ gcloud container clusters create \
        --machine-type n1-standard-1 \
        --num-nodes 1 \
        --zone europe-west1-d \
        --cluster-version latest \
        your-cluster-name

We choose the default cluster configuration (n1-standard-1) but you can choose another config if you want.
The cluster name is 'your-cluster-name' and the number of nodes is 1 as we will just try things but in a production environment you gonna upgrade this parameter.
Notice that you have to change the zone id to match your zone.

well now you can connect to your cluster using this command :

    $ gcloud container clusters get-credentials [your-cluster-name] --zone [your-zone] --project [your-project-id]

you can verify your context using this command :

    $ kubectl config current-context

## Best practices

### Image policy

We gonna set the image pull policy to always. So each time we restart the pod, it's gonna pull the last pushed image otherwise it will use the image already present locally (if exists).

- [image policy](https://kubernetes.io/docs/concepts/configuration/overview/#container-images)

### Image tags

You should avoid using the :latest tag when deploying containers in production as it is harder to track which version of the image is running and more difficult to roll back properly.

For each version, we gonna use a tag.

## Common installation

In some examples we will install some third party resources, so we will explain how to install them in this section.

### Create a global static ip

It's a good practice to reserve a static external ip from Google Cloud. If you change your services or even delete your cluster you will, the internet address to access to your services will not change.

To create a new global ip, use this command :

    $ gcloud compute addresses create my-web-static-ip --global

To check if the address has been created, use this command :

    $ gcloud compute addresses list

```log
NAME              ADDRESS/RANGE  TYPE      PURPOSE  NETWORK  REGION  SUBNET  STATUS
my-web-static-ip  34.98.92.51    EXTERNAL                                    RESERVED
```

### Create a regional static ip

In some cases, you need to reserve a regional static ip. So that, when you delete your cluster you can use it in an another service of another cluster.
Why we can't use the Global static ip ?

Because GKE Load balancer does not support the Global type. In our examples, when we will use the nginx ingress we gonna use this address as the ingress ip so that we don't have to reconfigure our domain name. check the example in watch-shop-nginx project for more details.

these links will help you to understand :

- https://stackoverflow.com/questions/46119406/error-creating-load-balancer-with-static-ip-on-gke-with-stable-traefik-chart
- https://cloud.google.com/kubernetes-engine/docs/tutorials/configuring-domain-name-static-ip?hl=en

To create a new regional ip, run this command (and change your zone) :

    $ gcloud compute addresses create europe-ip --region europe-west1

Verification :

    $ $ gcloud compute addresses list

```log
NAME              ADDRESS/RANGE  TYPE      PURPOSE  NETWORK  REGION        SUBNET  STATUS
my-web-static-ip  34.98.92.51    EXTERNAL                                          RESERVED
europe-ip         35.187.2.146   EXTERNAL                    europe-west1          IN_USE
```

Please not that your regional ip and your cluster must be in the same zone.

### Install nginx

To install the nginx ingress on your GKE cluster, use these commands :

    $ kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/static/mandatory.yaml

    $ kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/static/provider/cloud-generic.yaml

To install it on an another cloud platform, check this url :

- [nginx ingress](https://kubernetes.github.io/ingress-nginx/deploy/#prerequisite-generic-deployment-command)

```sh
    $ To check if the ingress controller pods have started, run the following command:
```

    $ kubectl get pods --all-namespaces -l app.kubernetes.io/name=ingress-nginx

```log
| NAMESPACE     | NAME                                      | READY | STATUS  | RESTARTS | AGE |
| ------------- | ----------------------------------------- | ----- | ------- | -------- | --- |
| ingress-nginx | nginx-ingress-controller-7dcc95dfbf-n9kmg | 1/1   | Running | 0        | 75s |
```

Once the operator pods are running, you can cancel the above command by typing Ctrl+C. Now, you are ready to create your first ingress.

### Install Cert Manager

For a custom installation, check the documentation :

- [install cert-manager](https://cert-manager.io/docs/installation/kubernetes/#installing-with-regular-manifests)

check that you are in the wright cluster :

    $ kubectl config current-context

As admin of the cluster (i'm using gke), execute these commands to install cert manager :

    $ kubectl create namespace cert-manager

    $ kubectl apply --validate=false -f https://github.com/jetstack/cert-manager/releases/download/v0.12.0-beta.1/cert-manager.yaml

Verification :

    $ kubectl get pods --namespace cert-manager --watch

wait until the pods became on a Running state then click ctrl+C.

```log
| NAME                                     | READY | STATUS  | RESTARTS | AGE |
| ---------------------------------------- | ----- | ------- | -------- | --- |
| cert-manager-787bfd954b-dz47q            | 1/1   | Running | 0        | 10s |
| cert-manager-cainjector-6d94d588f9-twxvb | 1/1   | Running | 0        | 10s |
| cert-manager-webhook-77c497f778-22zf9    | 1/1   | Running | 0        | 10s |
```

### Install Helm

Well, when i installed helm the first i had this error each time i deploy a pod :

```log
Events:
  Type     Reason            Age                 From               Message
  ----     ------            ----                ----               -------
  Warning  FailedScheduling  80s (x5 over 5m7s)  default-scheduler  0/1 nodes are available: 1 Insufficient cpu.
```

So, if you gonna install helm on your cluster it's better to use a n1-standard-2 cluster.

    $ gcloud container clusters create \
        --machine-type n1-standard-2 \
        --num-nodes 1 \
        --zone europe-west1-d \
        --cluster-version latest \
        your-cluster-name

To install Helm on GKE, we gonna download an sh script and run it on the cluster. You can't use the google SDK because the downloaded file gonna be on you local machine an not on the cluster. So, you have to run these commands using the shell provided by the google console :

    $ curl -o get_helm.sh https://raw.githubusercontent.com/kubernetes/helm/master/scripts/get
    $ chmod +x get_helm.sh
    $ ./get_helm.sh

Init Helm

    $ kubectl create serviceaccount --namespace kube-system tiller
    $ kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller
    $ helm init --service-account tiller

Verify it :

    $ kubectl get deployments -n kube-system

#### Install nginx using helm

always the the google console, run this command :

    $ helm install --name nginx-ingress stable/nginx-ingress --set rbac.create=true --set controller.publishService.enabled=true --set controller.service.loadBalancerIP=<YOUR_EXTERNAL_IP>

which is in my case :

    $ helm install --name nginx-ingress stable/nginx-ingress --set rbac.create=true --set controller.publishService.enabled=true --set controller.service.loadBalancerIP=35.187.2.146

![cloud-shell](/uploads/81a173e16bec8347a0d183765489c9bd/cloud-shell.png)

verify it :

    $ kubectl get service nginx-ingress-controller

![verify-it](/uploads/3ff77eec86d65dc6df1f15b326d9644f/verify-it.png)

### Configure a domain name

To deploy the https examples on your cluster, you have to have a domain name. In my case, i will use `mamdouni-apps.com` which i bought it from google domains and it costs 12 euro per year. Check the net if you want a cheaper domain name.

You must configure two records on this domain. an A record (naked domain) to redirect all your-domain.com to the ingress ip address and a CNAME record to redirect the www requests to your domain.

check the picture below :

![Selection_123](/uploads/e1bd58aa240872b5a14b8bdd7ac045b9/Selection_123.png)

point it to your ingress ip address (34.98.92.51). You can find the configuration under the DNS section of your domain (if you gonna use google domains). It takes some minutes with google domains to the changes to takes effects.

In the case you would like to use the regional ip address, do the same thing.

In my case it's gonna be :

![dns-regional](/uploads/ad55f62a56fd188efb8602fe265c3cd8/dns-regional.png)

## Some common errors

### QUOTA_EXCEEDED

Sometimes i face this error (using GKE always) :

- Error during sync: error running backend syncing routine: googleapi: Error 403: QUOTA_EXCEEDED - Quota 'BACKEND_SERVICES' exceeded. Limit: 5.0 globally.

Because of deleting and recreating services, some of them will not be really removed (i think that this is a google issue). So to clean up you cluster use these commands :

    $ gcloud compute backend-services list

```log
NAME                            BACKENDS                                                PROTOCOL
k8s-be-30366--0e4a1d7c33087320  europe-west1-d/instanceGroups/k8s-ig--0e4a1d7c33087320  HTTP
k8s-be-31440--0e4a1d7c33087320  europe-west1-d/instanceGroups/k8s-ig--0e4a1d7c33087320  HTTP
k8s-be-31695--a1017db58c1f86db  europe-west1-d/instanceGroups/k8s-ig--a1017db58c1f86db  HTTP
```

Well i have deleted all services and deployments from my cluster. However, there's something that can't be deleted.

Use this command to delete these services :

    $ gcloud compute backend-services delete [backend-name] --global

This is true when you a few services, but with more than 2 services the problem will not be fake and you have to contact google to increase you quota.

## Cleaning up

### Nginx ingress controller

To delete the nginx installation from your cluster, delete its namespace :

    $ kubectl delete namespace ingress-nginx

### Cert Manager

To delete cert-manager from the cluster, use this command :

    $ kubectl delete -f https://github.com/jetstack/cert-manager/releases/download/v0.12.0-beta.1/cert-manager.yaml

### Delete the nginx controller installed from helm

just delete the chart :

    $ helm del --purge nginx-ingress

### Helm

To remove helm installation from cluster, use these commands :

    $ kubectl -n kube-system delete deployment tiller-deploy && \
        kubectl -n kube-system delete serviceaccount tiller

### Cluster

To delete the cluster and its objects (service, ingress, deployment ...), use this command :

    $ gcloud container clusters delete [your-cluster-name]

![kubernetes](/uploads/6c2c5fea7381cd8b6cb2cd180df32ca9/kubernetes.png)

---

## Authors

- Mohamed Ali AMDOUNI
