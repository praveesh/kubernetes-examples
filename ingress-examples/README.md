# Ingress Examples

## Description

In this project, we will go deep into the kubernetes ingress component. We will explain how it works and what's the difference between the various implementation and the best practices. We will provide example about :

- how to deploy multiple apps on ingress
- how to configure the routes
- google ingress works
- difference between the google and the nginx ingress

and more ...
