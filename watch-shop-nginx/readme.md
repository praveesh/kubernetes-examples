# Https NGINX

## Description

In this example we will deploy the watch shop project on the google cloud using the nginx ingress. You can find the source code of the project on my gitlab :

- [watch shop](https://gitlab.com/dummies-apps/watch-shop)

The watch shop contains three sub projects :

- UI : developed using react js
- Backend : this app will expose a rest api used by the UI. We have two backends, the first supports only the http and the second supports the http and the https protocols (using a self signed certificate). In this example, we gonna use the second backend.

To install the SSL certificate, we gonna use the same configuration as in the project ingress-examples -> 4_https-nginx-http01

## Getting Started

Before starting the example, i will show you what is the expected result and how the configure the context of the deployed apps.

### Routes

We gonna use the google ingress to deploy our app and here's the resulted architecture.

![ingress-nginx-architecture](/uploads/6b3d9f2368d80e9847f710c65d16d9b5/ingress-nginx-architecture.jpg)

### Push the back docker image

We gonna clone the back repository and push it to the gcr :

    $ git clone https://gitlab.com/dummies-apps/watch-shop/watch-shop-backend-https.git

    $ cd watch-shop-backend-https/

Now we gonna build and push the docker image to the gcr (google container registry). Check the ingress-examples -> show-url-node-app to learn more about how to push a docker image.

    $ docker build -t eu.gcr.io/[YOUR-PROJECT-ID]/watch-shop-backend-ssl:v1.0.0 .

    $ gcloud docker -- push eu.gcr.io/[YOUR-PROJECT-ID]/watch-shop-backend-ssl:v1.0.0

You can check this url to see if the image has been pushed correctly :

- [google container registry](https://console.cloud.google.com/gcr)

### Push the front docker image

Clone the front repository and change its sources to add the context.

    $ git clone https://gitlab.com/dummies-apps/watch-shop/watch-shop-frontend.git

    $ cd watch-shop-frontend

Open the `src/constants/constants.js` with a text editor. I will use nano in my case.

    $ nano src/constants/constants.js

Change the API_URL to match the url of the backend endpoint. http://35.187.2.146/back in our case.
Change the USE_MOCKS to false.

As we gonna configure our custom readiness probes and liveness probes (this config is already done on the backend). We will add these probes to our server (which is nginx in our case).

Open the `docker/nginx/nginx.conf` file with a text editor.

    $ nano docker/nginx/nginx.conf

And add these lines :

```nginx
location = /healthz {
            # health check endpoint
            return 200 '{"status":"up"}';
            add_header Content-Type text/json;
        }
```

![nginx-config-2](/uploads/8eaf562f68af49f35a28fc9bb606b7b2/nginx-config-2.png)

As our context is /front we gonna add it to the package config file otherwise our front will try to get js scripts from the root. To do so, open `package.json` :

    $ nano package.json

and upgrade the homepage property from "." to "./front" :

```json
{
  "name": "watch-shop-frontend",
  "version": "0.1.0",
  "private": true,
  "homepage": "./front",
  "dependencies": {
    "@testing-library/jest-dom": "^4.2.4",
    "@testing-library/react": "^9.4.0",
    "@testing-library/user-event": "^7.2.1",
    "axios": "^0.19.0",
    "react": "^16.12.0",
    "react-dom": "^16.12.0",
    "react-router-dom": "^5.1.2",

```

To build the image, run these commands :

    $ docker build -t eu.gcr.io/[YOUR-PROJECT-ID]/watch-shop-frontend:v1.0.0 .

    $ gcloud docker -- push eu.gcr.io/[YOUR-PROJECT-ID]/watch-shop-frontend:v1.0.0

You can check this url to see if the image has been pushed correctly :

- [google container registry](https://console.cloud.google.com/gcr)

## Installation

### Http

Before starting, you need to have :

1. helm installed on your cluster
2. nginx installed on your cluster using helm
3. cert-manager installed on your cluster
4. configure your domain name to point to your google regional static ip

Check the main readme to install them.

**PN** : That we can't use the global static ip in this example. Helm will create a Load Balancer as an nginx-controller and we can't assign a global ip the a Load Balancer (GKE does not support it) so we will use the regional ip.

On the current directory, type this command :

    $ kubectl apply -f back-app.yaml && \
        kubectl apply -f front-app.yaml && \
        kubectl apply -f my-ingress.yaml

For the moment, my ingress configuration is like below :

```yaml
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: my-ingress
  annotations:
    kubernetes.io/ingress.class: nginx
    nginx.ingress.kubernetes.io/rewrite-target: /$2
spec:
  rules:
    - http:
        paths:
          - path: /front(/|$)(.*)
            backend:
              serviceName: frontend-cluster-ip
              servicePort: 3000
          - path: /back(/|$)(.*)
            backend:
              serviceName: backend-cluster-ip
              servicePort: 80
```

In this ingress definition, any characters captured by (.\*) will be assigned to the placeholder \$2 ( (/|\$) is the first group and (.\*) is the second ), which is then used as a parameter in the rewrite-target annotation.

For example, the ingress definition above will result in the following rewrites:

rewrite.bar.com/back rewrites to rewrite.bar.com/
rewrite.bar.com/back/ rewrites to rewrite.bar.com/
rewrite.bar.com/back/new rewrites to rewrite.bar.com/new

- https://kubernetes.github.io/ingress-nginx/examples/rewrite/#rewrite-target

Check pods :

    $ kubectl get pods

```log
NAME                                             READY   STATUS    RESTARTS   AGE
backend-deployment-77d5887df8-bhw6t              1/1     Running   0          34s
frontend-deployment-6fb67bf576-jwvzc             1/1     Running   0          27s
nginx-ingress-controller-57668b5c4c-s9m2w        1/1     Running   0          16m
nginx-ingress-default-backend-576b86996d-w8std   1/1     Running   0          16m
```

Check services :

    $ kubectl get services

```log
NAME                            TYPE           CLUSTER-IP      EXTERNAL-IP    PORT(S)                      AGE
backend-cluster-ip              ClusterIP      10.55.241.59    <none>         443/TCP,80/TCP               19m
frontend-cluster-ip             ClusterIP      10.55.247.202   <none>         3000/TCP                     19m
kubernetes                      ClusterIP      10.55.240.1     <none>         443/TCP                      17h
nginx-ingress-controller        LoadBalancer   10.55.251.156   35.187.2.146   80:32491/TCP,443:31157/TCP   21m
nginx-ingress-default-backend   ClusterIP      10.55.248.150   <none>         80/TCP                       21m
```

Check ingress :

    $ kubectl get ingress

```log
NAME         HOSTS   ADDRESS        PORTS   AGE
my-ingress   *       35.187.2.146   80      21m
```

### Test Http endpoints

    $ curl http://35.187.2.146/front/healthz

```log
{"status":"up"}
```

    $ curl http://35.187.2.146/back/healthz

```json
{ "status": "UP", "route": "http://35.187.2.146/healthz" }
```

    $ curl http://35.187.2.146/back/gifts

```log
[{"name":"Happy Birthday","price":"25$","description":"Amazon.com eGift Card.","image":"https://m.media-amazon.com/images/I/81gQraBrBZL._SS500_.jpg","details":{"Type":"Digital gift card","Custom message":"Yes","Delivery method":"Email","Gift amount":"$1 - $2,000"}},{"name":"Welcome Baby","price":"15$","description":"Amazon.com eGift Card.","image":"https://cdn5.vectorstock.com/i/1000x1000/35/79/welcome-baby-card-vector-14713579.jpg","details":{"Type":"Digital gift card","Custom message":"No","Delivery method":"Mail","Gift amount":"$1 - $4,000"}},{"name":"Congratulations","price":"7$","description":"Amazon.com eGift Card.","image":"https://cdn.shopify.com/s/files/1/0786/6519/products/congrats_on_sleeping_with_the_same_person_wedding_card.jpg?v=1532655560","details":{"Type":"Digital gift card","Custom message":"Yes","Delivery method":"PDF download","Gift amount":"$1 - $6,000"}},{"name":"Thank you","price":"6.99$","description":"Amazon.com eGift Card.","image":"https://fiverr-res.cloudinary.com/images/t_main1,q_auto,f_auto/gigs/108897597/original/2a21b24798a3c3fe4a492ff52ab8ec52b86bc71b/design-awesome-thank-you-card-within-6-hours.jpg","details":{"Type":"Digital gift card","Custom message":"Yes","Delivery method":"Email","Gift amount":"$1 - $200"}}]
```

    $ curl http://mamdouni-apps.com/front/healthz

```json
{ "status": "up" }
```

    $ curl http://www.mamdouni-apps.com/back/healthz

```json
{ "status": "UP", "route": "http://www.mamdouni-apps.com/healthz" }
```

If you want to test the front app, use this link :

- [http://www.mamdouni-apps.com/front](http://www.mamdouni-apps.com/front)

![front-http](/uploads/a08380f5ce0e186ad82f156d3acd7115/front-http.png)

Now, we gonna secure our app now using the ssl now.

### Configure SSL

On the current directory, type this command :

    $ kubectl apply -f letsencrypt-issuer.yaml && \
        kubectl apply -f my-certificate.yaml

Check the issuer :

    $ kubectl get clusterissuer letsencrypt-production -o wide

```log
NAME                     READY   STATUS                                                 AGE
letsencrypt-production   True    The ACME account was registered with the ACME server   27s
```

Check the certificate :

    $ kubectl get certificate mamdouni-apps-certificate -o wide

```log
NAME                        READY   SECRET                  ISSUER                   STATUS                                          AGE
mamdouni-apps-certificate   True    mamdouni-apps-com-tls   letsencrypt-production   Certificate is up to date and has not expired   50s
```

After some seconds this time. We can see that the certificate is ready :

    $ kubectl describe certificate mamdouni-apps-certificate

```log
Status:
  Conditions:
    Last Transition Time:  2020-01-09T11:03:43Z
    Message:               Certificate is up to date and has not expired
    Reason:                Ready
    Status:                True
    Type:                  Ready
  Not After:               2020-04-08T10:03:42Z
Events:
  Type    Reason        Age   From          Message
  ----    ------        ----  ----          -------
  Normal  GeneratedKey  110s  cert-manager  Generated a new private key
  Normal  Requested     110s  cert-manager  Created new CertificateRequest resource "mamdouni-apps-certificate-3247189968"
  Normal  Issued        74s   cert-manager  Certificate issued successfully

```

Now we have to add the certificate stored in the secret to our ingress file and changes the routes. The ingress configuration will be like this :

```yaml
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: my-ingress
  annotations:
    kubernetes.io/ingress.class: nginx
    nginx.ingress.kubernetes.io/rewrite-target: /$2
    nginx.ingress.kubernetes.io/ssl-redirect: "true"
spec:
  tls:
    - hosts:
        - mamdouni-apps.com
        - www.mamdouni-apps.com
      secretName: mamdouni-apps-com-tls
  rules:
    - host: mamdouni-apps.com
      http:
        paths:
          - path: /front(/|$)(.*)
            backend:
              serviceName: frontend-cluster-ip
              servicePort: 3000
          - path: /back(/|$)(.*)
            backend:
              serviceName: backend-cluster-ip
              servicePort: 80
    - host: www.mamdouni-apps.com
      http:
        paths:
          - path: /front(/|$)(.*)
            backend:
              serviceName: frontend-cluster-ip
              servicePort: 3000
          - path: /back(/|$)(.*)
            backend:
              serviceName: backend-cluster-ip
              servicePort: 80
```

We can apply the changes now :

    $ kubectl apply -f my-ingress.yaml

### Modify the config files

We have to modify our front config file now because we are using https and our domain name in the ingress file.

Go to the downloaded source of the frontend. Open the `src/constants/constants.js` with a text editor. I will use nano in my case.

    $ nano src/constants/constants.js

Change the API_URL to match the url of the backend endpoint, https://mamdouni-apps.com/back in our case.

Rebuild and re-push the image to the grc. As we will not change the image tag, we have to delete the frontend pod to re-pull the image from the gcr.

    $ kubectl get pods

```log
NAME                                             READY   STATUS    RESTARTS   AGE
backend-deployment-77d5887df8-2hdvp              1/1     Running   0          130m
frontend-deployment-6fb67bf576-jwgkg             1/1     Running   0          63m
nginx-ingress-controller-57668b5c4c-bjpsm        1/1     Running   0          132m
nginx-ingress-default-backend-576b86996d-ckgxb   1/1     Running   0          132m
```

    $ kubectl delete pod frontend-deployment-6fb67bf576-jwgkg

```log
pod "frontend-deployment-6fb67bf576-jwgkg" deleted
```

### Test Https endpoints

Now we can test our SSL connection.

    $ curl https://mamdouni-apps.com/front/healthz

```json
{ "status": "up" }
```

    $ curl https://www.mamdouni-apps.com/back/healthz

```json
{ "status": "UP", "route": "http://www.mamdouni-apps.com/healthz" }
```

    $ curl http://www.mamdouni-apps.com/back/healthz

```html
<html>
  <head>
    <title>308 Permanent Redirect</title>
  </head>
  <body>
    <center><h1>308 Permanent Redirect</h1></center>
    <hr />
    <center>openresty/1.15.8.2</center>
  </body>
</html>
```

We can't use the http anymore because we have used the ssl redirection annotation in the ingress file.

    $ curl https://mamdouni-apps.com/back/gifts

```json
[
  {
    "name": "Happy Birthday",
    "price": "25$",
    "description": "Amazon.com eGift Card.",
    "image": "https://m.media-amazon.com/images/I/81gQraBrBZL._SS500_.jpg",
    "details": {
      "Type": "Digital gift card",
      "Custom message": "Yes",
      "Delivery method": "Email",
      "Gift amount": "$1 - $2,000"
    }
  },
  {
    "name": "Welcome Baby",
    "price": "15$",
    "description": "Amazon.com eGift Card.",
    "image": "https://cdn5.vectorstock.com/i/1000x1000/35/79/welcome-baby-card-vector-14713579.jpg",
    "details": {
      "Type": "Digital gift card",
      "Custom message": "No",
      "Delivery method": "Mail",
      "Gift amount": "$1 - $4,000"
    }
  },
  {
    "name": "Congratulations",
    "price": "7$",
    "description": "Amazon.com eGift Card.",
    "image": "https://cdn.shopify.com/s/files/1/0786/6519/products/congrats_on_sleeping_with_the_same_person_wedding_card.jpg?v=1532655560",
    "details": {
      "Type": "Digital gift card",
      "Custom message": "Yes",
      "Delivery method": "PDF download",
      "Gift amount": "$1 - $6,000"
    }
  },
  {
    "name": "Thank you",
    "price": "6.99$",
    "description": "Amazon.com eGift Card.",
    "image": "https://fiverr-res.cloudinary.com/images/t_main1,q_auto,f_auto/gigs/108897597/original/2a21b24798a3c3fe4a492ff52ab8ec52b86bc71b/design-awesome-thank-you-card-within-6-hours.jpg",
    "details": {
      "Type": "Digital gift card",
      "Custom message": "Yes",
      "Delivery method": "Email",
      "Gift amount": "$1 - $200"
    }
  }
]
```

If you want to see the certificate, try these links :

- [https://mamdouni-apps.com/back/deals](https://mamdouni-apps.com/back/deals)

![watch-certificate-nginx](/uploads/fd18231c38041e33ae2e8e28755f964d/watch-certificate-nginx.png)

- [https://www.mamdouni-apps.com/front](https://www.mamdouni-apps.com/front)

![watch-certificate-nginx2](/uploads/167412ff7e45cb623b936fdd149c9d22/watch-certificate-nginx2.png)

## Cleaning up

To clean up your cluster, use this command :

    $ kubectl delete -f .
